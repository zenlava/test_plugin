# Membrane_cell_ratio.py
## Description

A Fiji plugin extracting the ratio between the mean intensity of the membrane and the interior part of the cell.
The plugin uses Li's Minimum Cross Entropy thresholding method on the focal planes maximum projection to segment the cells, applies a Gaussian filter and then separates them using a watershed algorithm.
Starting from these cells borders, the membrane of each cell is identified as the region with a thickness of 1.5um.
The mean fluorescence of the membrane and of the interior part of the cell is evaluated on the focal plane where the membrane has maximum mean intensity.
Then the ratio is calculated.

## How to use the plugin

- Open a z-stack in Fiji
- Drag and drop the file containing the plugin in the toolbar and click "Run"

A dialog window will pop up:

![alt text] (https://bitbucket.org/zenlava/test_plugin/downloads/Screenshot_2021-02-16_at_16.18.38.png)

- Select the condition for the experiment
- Select the radius for subtracting the background to the images (default value = 20)
- Set the sigma radius for gaussian blur, helpful for cell segmentation (default value = 3)
- Set membrane thickness, in microns (default value 0.45)
- Set cell area in microns - minimum value (default = 5)
- Set cell area in microns - maximum value (default = 100000000)

Click Ok and wait for the plugin to calculate the results. 

## Output

The list of ROIs will appear in the ROI manager and the ROIs will be shown superimposed to the image. 

![alt text] (https://bitbucket.org/zenlava/test_plugin/downloads/Picture_2.png)

Additionally, a Result Table with the area and mean intensity of the ROI, and the calculated ratio between the mean intensity of the membrane and the interior part of the cell will pop up. 

![alt text] (https://bitbucket.org/zenlava/test_plugin/downloads/Picture_3.png)

## Installation

The plugin does not require any installation. Just drag and drop the file containing the plugin in the FIJI toolbar and click "Run".


## Development

Image Analysis Team @ [IFOM](https://www.ifom.eu/en/cancer-research/technological-units/imaging.php) , Milano




**“Free software” is a matter of liberty, not price. To understand the concept, you should think of “free” as in “free speech,” not as in “free beer”.**

