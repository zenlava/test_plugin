#@ImagePlus imp
#@String(label="Please enter a condition for the experiment",value = "control") condition
#@Integer(label="background correction radius", value = 20) r_bg
#@Integer(label="sigma blurring radius for cell recognition", value = 3) sigma_blur
#@Float(label="membrane thickness in micron", value = 0.45) enlargment_um
#@Float(label="cell minimum area micron", value = 5) min_area_um
#@Float(label="cell maximum area micron", value = 100000000) max_area_um


''' 2017_11_27 emanuele.martini00@gmail.com

get Membrane/Central region cells ratio

Cell identified using SUM projection
Membrane as shrinking ring from cell 
"interior" part of the Cell as Cell - Membrane 

signal is evaluated on the plane where the mean intensity of membrane is maximum

'''

from ij import IJ, WindowManager
from ij.plugin.filter import ParticleAnalyzer as PA
from ij.plugin.frame import RoiManager
from ij.plugin import ZProjector
from ij.measure import ResultsTable, Measurements
from ij.plugin import Duplicator, ImageCalculator
from ij.process import ImageStatistics
from ij.text import TextWindow
from java.awt import Color
import math
import os

def getCells_rois(imp,sigma_blur,min_area_um,max_area_um):
	
	Z_proj = ZProjector();
	Z_proj.setImage(imp)
	Z_proj.setMethod(ZProjector.MAX_METHOD);
	Z_proj.doProjection()
	sum_proj = Z_proj.getProjection();
	
	IJ.run(sum_proj, "Gaussian Blur...", "sigma="+str(sigma_blur)+"");
	#IJ.run(sum_proj, "Subtract Background...", "rolling="+str(r_bg)+"");
	IJ.setAutoThreshold(sum_proj, "Li dark");
	IJ.run(sum_proj, "Convert to Mask", "");
	IJ.run(sum_proj, "Watershed", "");
	calib =imp.getCalibration()
	um_to_pixel= 1/calib.getX(1)
	
	min_area_pix = min_area_um*(um_to_pixel*um_to_pixel)
	max_area_pix = max_area_um*(um_to_pixel*um_to_pixel)
	#min_area_um = imp.getCalibration().getRawX()
	IJ.run(sum_proj, "Analyze Particles...", "size="+str(min_area_pix)+"-"+str(max_area_pix)+" pixel exclude add");	

def getMembraneRois_and_measure(imp,rm, enlargment_um,rt_membrane,condition):
	roi_to_delete = []	
	
	cell_rois = rm.getCount();
	num_Z = imp.getNSlices()
	membrane_z_mean_sum = [0]*num_Z; #for each z-stack the sum of the mean membrane intensities is measured and summed up
	rt_best_plane = ResultsTable();
	for i in range(0,cell_rois):		
		roi = rm.getRoi(i)
		
		rm.runCommand(imp,"Show None");					
		imp.setRoi(roi)
		

		IJ.run(imp, "Enlarge...", "enlarge=-"+str(enlargment_um)+"");
		enlarged_roi = imp.getRoi();
		enlarged_roi.setName("cell_"+str(i+1))
		enlarged_roi.setStrokeColor(Color.BLUE);		
		rm.addRoi(enlarged_roi);		
		
		rm.runCommand(imp,"Deselect");
		
		
		to_combine = [];
		to_combine.append(i)
		to_combine.append(2*i+cell_rois)
		
		rm.setSelectedIndexes(to_combine)
		rm.runCommand(imp,"XOR");
		membrane_roi = imp.getRoi()
		membrane_roi.setName("membrane_"+str(i+1))
		membrane_roi.setStrokeColor(Color.GREEN);
		rm.addRoi(membrane_roi);
		rt_best_plane.incrementCounter()
		rt_best_plane.addValue("cell_name",enlarged_roi.getName())
		# get best plane as the plane where the mean intensity of membrane is maximum
		for z in range(1,num_Z):			
			imp.setSlice(z)
			imp.setRoi(enlarged_roi)
			area_z, mean_z, std_z = getMeasurements(membrane_roi,imp)		 	
		 	rt_best_plane.addValue("Z_"+str(z),mean_z)
			
			


		roi_to_delete.append(i);
	
	rm.setSelectedIndexes(roi_to_delete)
	rm.runCommand(imp,"Delete")
	
	rt_best_plane.incrementCounter();	
	for z in range(1,num_Z):
		z_values = rt_best_plane.getColumnAsDoubles(z)
		membrane_z_mean_sum[z-1] = sum(z_values)			
		rt_best_plane.addValue("Z_"+str(z),membrane_z_mean_sum[z-1])	
		max_mean_membranes = max(membrane_z_mean_sum)
		best_plane = membrane_z_mean_sum.index(max_mean_membranes)+1

	#measurements 
	imp.setSlice(best_plane)
	rt_best_plane.reset();	
	for i in range(0,rm.getCount()-1,2):
		rt_membrane.incrementCounter()		
		rt_membrane.addValue("image",imp.getTitle())		
		rt_membrane.addValue("condition",condition)
		
		enlarged_roi = rm.getRoi(i)
		enlarged_roi.setPosition(best_plane)
		area_en,mean_en,std_en = getMeasurements(enlarged_roi,imp)
		rt_membrane.addValue("cell_name",enlarged_roi.getName())
		membrane_roi =rm.getRoi(i+1)
		membrane_roi.setPosition(best_plane)
		rm.runCommand(imp,"Deselect");
		rm.runCommand(imp,"Select None")	
		area_mem,mean_mem,std_mem = getMeasurements(membrane_roi,imp)
		rm.runCommand(imp,"Deselect");
		rm.runCommand(imp,"Select None")	

		area_tot = area_mem+area_en
		rt_membrane.addValue("area_Cell_and_Membrane",area_tot)
		rt_membrane.addValue("area_membrane", area_mem)
		rt_membrane.addValue("area_cell", area_en) 
		rt_membrane.addValue("mean_cell",mean_en)
		rt_membrane.addValue("mean_membrane",mean_mem)
		rt_membrane.addValue("ratio_meanMembrane_Cell",mean_mem/mean_en)
		rt_membrane.addValue("plane_selected",best_plane)

	return best_plane
	'''

	'''
	
	


	#print(roi_to_delete)
		
def getMeasurements(roi,imp_to_measure):
	imp_to_measure.setRoi(roi)
	stats = imp_to_measure.getStatistics()
	area = stats.area
	mean = stats.mean
	std = stats.stdDev	
	return area,mean,std
	
				
		
def run_script():
    rm = RoiManager().getInstance()
    if rm != None :
    	rm.close();
	rm = RoiManager();	  
	#imp = WindowManager.getCurrentImage()
	#crop out deconvolution artifacts on the borders
	imp.setRoi(50,48,927,926); 
	IJ.run(imp, "Crop", "");


	# get cells rois using sum projection
	#sigma_blur = 3 
	
	getCells_rois(imp,sigma_blur,min_area_um,max_area_um)
	
	# get Membrane and measure on MAX proj
	rt_exist = WindowManager.getWindow("Membrane_ratio_Results")
	if rt_exist==None or not isinstance(rt_exist, TextWindow):
		rt_membrane = ResultsTable()
	else:
		rt_membrane = rt_exist.getTextPanel().getOrCreateResultsTable()

	rt_membrane.show("Membrane_ratio_Results")


	#enlargment_um = 0.45
	#condition = "test"
	#bg subtraction 	
	IJ.run(imp, "Subtract Background...", "rolling="+str(r_bg)+" sliding stack");
	best_plane = getMembraneRois_and_measure(imp,rm,enlargment_um,rt_membrane,condition)
	rt_membrane.show("Membrane_ratio_Results")	
	imp.setSlice(best_plane)
	rm.runCommand("Associate", "true");
	rm.runCommand("Centered", "false");
	rm.runCommand("UseNames", "true");
	rm.runCommand(imp,"Show All");

	
if __name__ in ['__builtin__', '__main__']:
    run_script();